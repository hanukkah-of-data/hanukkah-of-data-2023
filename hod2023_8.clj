(ns hod2023_8
  (:require [clojure.java.jdbc :as jdbc]
            [honey.sql :as sql]
            [honey.sql.helpers :as h]))

(def spec
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "noahs.sqlite"})

(def collectible-orders
  (-> (h/from [:customers :c])
      (h/join [:orders :o] [:= :c.customerid :o.customerid])
      (h/join [:orders_items :oi] [:= :o.orderid :oi.orderid])
      (h/join [:products :p] [:= :p.sku :oi.sku])
      (h/select :c.customerid
                :c.name
                :c.phone
                [[:count [:distinct :p.sku]] :prod-count])
      (h/where [:like :p.sku "COL%"])
      (h/group-by :c.customerid
                  :c.name
                  :c.phone)
      (h/having [:= 85 [:count [:distinct :p.sku]]])
      (h/limit 300)
      (sql/format {:inline true})))

(jdbc/query spec collectible-orders)
