(ns hod2023_7
  (:require [clojure.java.jdbc :as jdbc]
            [honey.sql :as sql]
            [honey.sql.helpers :as h]))

(def spec
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "noahs.sqlite"})

(def sherri-orders
  (-> (h/select :c.customerid
                :c.name
                :p.sku
                :p.desc
                :o.ordered)
      (h/from [:customers :c])
      (h/join [:orders :o] [:= :c.customerid :o.customerid])
      (h/join [:orders_items :oi] [:= :o.orderid :oi.orderid])
      (h/join [:products :p] [:= :p.sku :oi.sku])
      (h/where [:= :o.customerid 4167]
               [:= :o.ordered :o.shipped])
      (h/where [:or
                [:like :p.desc "Noah's Poster %"]
                [:like :p.desc "Noah's Lunchbox %"]
                [:like :p.desc "Noah's Jewelry %"]
                [:like :p.desc "Noah's Jersey %"]
                [:like :p.desc "Noah's Gift Box %"]
                [:like :p.desc "Noah's Bobblehead %"]
                [:like :p.desc "Noah's Action Figure %"]])
      (h/order-by :c.customerid)
      (h/limit 50)
      (sql/format {:inline true})))

(jdbc/query spec sherri-orders)

{:customerid 4167, :name "Sherri Long", :sku "COL7955", :desc "Noah's Action Figure (green)",
 :ordered "2020-06-28 11:36:06"}
{:customerid 4167, :name "Sherri Long", :sku "COL7063", :desc "Noah's Poster (azure)",
 :ordered "2018-12-31 12:26:58"}

(def same-day-sherri-orders
  (-> (h/select :c.customerid
                :c.name
                :c.phone
                :p.desc
                :o.ordered)
      (h/from [:customers :c])
      (h/join [:orders :o] [:= :c.customerid :o.customerid])
      (h/join [:orders_items :oi] [:= :o.orderid :oi.orderid])
      (h/join [:products :p] [:= :p.sku :oi.sku])
      (h/where [:!= :o.customerid 4167]
               [:not [:in :p.sku ["COL7955" "COL7063"]]]
               [:in [:date :o.ordered] ["2020-06-28" "2018-12-31"]]
               [:= :o.ordered :o.shipped])
      (h/where [:or
                [:like :p.desc "Noah's Poster %"]
                [:like :p.desc "Noah's Action Figure %"]])
      (h/order-by :c.customerid)
      (h/limit 50)
      (sql/format {:inline true})))

(jdbc/query spec same-day-sherri-orders)
