(ns hod2023_4
  (:require [clojure.java.jdbc :as jdbc]
            [honey.sql :as sql]
            [honey.sql.helpers :as h]))

(def spec
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "noahs.sqlite"})

(def bakery_orders_query
  (-> (h/select :c.customerid
                :c.name
                :c.phone
                [[:min [:time :o.ordered]] :min_order_time])
      (h/from [:customers :c])
      (h/join [:orders :o] [:= :c.customerid :o.customerid])
      (h/join [:orders_items :oi] [:= :o.orderid :oi.orderid])
      (h/where [:like :oi.sku "BKY%"]
               [:= :o.ordered :o.shipped])
      (h/group-by :c.customerid)
      (h/order-by :min_order_time)
      (h/limit 300)
      (sql/format {:inline true})))

(jdbc/query spec bakery_orders_query)
