(ns hod2023_1
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [honey.sql :as sql]
            [honey.sql.helpers :as h]))

(def spec
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "noahs.sqlite"})

;; (def cust_query
;;   "select name, replace(phone, '-', '') as phone from customers")

(def cust_query
  (-> (h/select [[:replace :phone "-" ""] :phone])
      (h/from [:customers])
      (sql/format {:inline true})))

(def customers (jdbc/query spec cust_query))

(def letter-to-num
  {:a "2" :b "2" :c "2"
   :d "3" :e "3" :f "3"
   :g "4" :h "4" :i "4"
   :j "5" :k "5" :l "5"
   :m "6" :n "6" :o "6"
   :p "7" :q "7" :r "7" :s "7"
   :t "8" :u "8" :v "8"
   :w "9" :x "9" :y "9" :z "9"})

((keyword "d") letter-to-num)

(defn last-name-to-num [last-name]
  (let [last-name-lower (str/lower-case last-name)
        last-name-count (count last-name)]
    (if (= 10 last-name-count)
      (map
       #((keyword %) letter-to-num)
       (str/split last-name-lower #""))
      nil)))

(str/split "js" #"")
(str/split "1234" #"")

(last-name-to-num "aaaaaaaaaa")

(defn name-eq-num? [cust]
  (let [name-split (str/split (:name cust) #" ")
        last-name (last name-split)
        last-name-num (last-name-to-num last-name)
        phone-split (str/split (:phone cust) #"")]
    (= last-name-num phone-split)))

(name-eq-num? {:name "aaaaaaaaaa" :phone "2222222222"})

(filter name-eq-num? customers)
