(ns hod2023_3
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [honey.sql :as sql]
            [honey.sql.helpers :as h]
            [tech.v3.dataset :as ds]
            [java-time.api :as jt]))

(def spec
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "noahs.sqlite"})

(def cust_query
  (-> (h/select [:*])
      (h/from [:customers])
      (sql/format {:inline true})))

(def customers (jdbc/query spec cust_query))

(def rabbit-years
  (ds/->dataset "years-of-the-rabbit.tsv"
                {:key-fn keyword
                 :parser-fn
                 {"start-date" [:local-date "d MMMM yyyy"]
                  "end-date" [:local-date "d MMMM yyyy"]}}))

(def customers-ds
  (ds/->dataset customers
                {:parser-fn {:birthdate :local-date}}))

(-> customers-ds
    (ds/filter-column :customerid 1293))

(defn compare-with-one-rabbit-year [birthdate rabbit-row]
  (and (jt/after? birthdate (:start-date rabbit-row))
       (jt/before? birthdate (:end-date rabbit-row))))

(defn is-rabbit-cancer [birthdate rabbit-ds]
  (some true? (pmap
               (partial compare-with-one-rabbit-year birthdate)
               (ds/rows rabbit-ds))))

(is-rabbit-cancer (jt/local-date 1963 7) rabbit-years)

(-> customers-ds
    (ds/row-map (fn [{birthdate :birthdate}]
                  {:is-rabbit-cancer
                   (is-rabbit-cancer birthdate rabbit-years)}))
    (ds/filter-column :is-rabbit-cancer true)
    (ds/filter-column :citystatezip #(str/starts-with? % "Jamaica, NY")))
