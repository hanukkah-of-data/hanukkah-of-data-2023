(ns hod2023_2
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]
            [honey.sql :as sql]
            [honey.sql.helpers :as h]))

(def spec
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "noahs.sqlite"})

(def cust_query
  (-> (h/select [:*])
      (h/from [:customers])
      (sql/format)))

(def customers (jdbc/query spec cust_query))

(def cust-names (map :name customers))

(map #(re-find #"\b(\w)" %) cust-names)

(re-seq #"((\d+)-(\d+))" "672-345-456-3212")
(re-seq #"\b\w" "John Smith")
(str/starts-with? "Manhattan, NY 0000" "Manhattan, NY")

(def customers-with-initials (map
                              #(assoc % :initials
                                      (re-seq #"\b\w" (:name %)))
                              customers))

(def jp-cust (filter #(= (:initials %) '("J" "P"))
                     customers-with-initials))

(def jp-cust-ids (pmap :customerid jp-cust))
(str/join ", " jp-cust-ids)

(def order_query_h
  (-> (h/select-distinct [:c.*])
      (h/from [:orders :o])
      (h/join [:orders_items :oi] [:= :o.orderid :oi.orderid])
      (h/join [:customers :c] [:= :c.customerid :o.customerid])
      (h/where [:= :oi.sku "HOM2761"]
               [:like :o.ordered "2017-%"]
               [:in :o.customerid jp-cust-ids])
      (sql/format {:inline true})))

(jdbc/query spec order_query_h)
