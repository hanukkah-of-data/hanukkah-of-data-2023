SELECT c.customerid, c.name, c.phone, COUNT(oi.sku) AS cat_prod_count
FROM customers AS c
INNER JOIN orders AS o ON c.customerid = o.customerid
INNER JOIN orders_items AS oi ON o.orderid = oi.orderid
INNER JOIN products AS p ON p.sku = oi.sku
WHERE (c.citystatezip LIKE 'Staten Island, NY%')
AND (p.desc LIKE '%Senior Cat%')
GROUP BY c.customerid, c.name, c.phone
ORDER BY COUNT(oi.sku) DESC
