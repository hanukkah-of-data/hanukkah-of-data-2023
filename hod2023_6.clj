(ns hod2023_6
  (:require [clojure.java.jdbc :as jdbc]
            [honey.sql :as sql]
            [honey.sql.helpers :as h]))

(def spec
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "noahs.sqlite"})

(def sum_orders_query
  (-> (h/select :c.customerid
                :c.name
                :c.phone
                [[:count :*] :cust-count])
      (h/from [:customers :c])
      (h/join [:orders :o] [:= :c.customerid :o.customerid])
      (h/join [:orders_items :oi] [:= :o.orderid :oi.orderid])
      (h/join [:products :p] [:= :p.sku :oi.sku])
      (h/where [:< :oi.unit_price :p.wholesale_cost])
      (h/group-by :c.customerid
                  :c.name
                  :c.phone)
      (h/order-by [[:count :*] :desc])
      (h/limit 300)
      (sql/format {:inline true})))

(jdbc/query spec sum_orders_query)
