(ns hod2023_5
  (:require [clojure.java.jdbc :as jdbc]
            [honey.sql :as sql]
            [honey.sql.helpers :as h]))

(def spec
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "noahs.sqlite"})

;; (def jersey_orders
;;   (-> (h/select :c.customerid
;;                 :c.name
;;                 :c.phone)
;;       (h/from [:customers :c])
;;       (h/join [:orders :o] [:= :c.customerid :o.customerid])
;;       (h/join [:orders_items :oi] [:= :o.orderid :oi.orderid])
;;       (h/join [:products :p] [:= :p.sku :oi.sku])
;;       (h/where [:like :p.desc "Noah's Jersey %"])
;;       (h/group-by :c.customerid)
;;       (sql/format {:inline true})))

;; (def bought_jersey (jdbc/query spec jersey_orders))

(def cat_orders
  (-> (h/select :c.customerid
                :c.name
                :c.phone
                ;; :p.desc
                [[:count :oi.sku] :cat-prod-count])
      (h/from [:customers :c])
      (h/join [:orders :o] [:= :c.customerid :o.customerid])
      (h/join [:orders_items :oi] [:= :o.orderid :oi.orderid])
      (h/join [:products :p] [:= :p.sku :oi.sku])
      (h/where
       [:like :c.citystatezip "Staten Island, NY%"]
       ;; [:in :c.customerid (map :customerid bought_jersey)]
       [:like :p.desc "%Senior Cat%"])
      (h/group-by :c.customerid :c.name :c.phone)
      (h/order-by [[:count :oi.sku] :desc])
      ;; (h/order-by :c.customerid)
      (sql/format {:inline true})))

(jdbc/query spec cat_orders)
